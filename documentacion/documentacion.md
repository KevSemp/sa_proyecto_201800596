# Documentación

<h2>Glosario</h2>

* <strong>Bases de datos relacional:</strong> es un conjunto de una o más tablas estructuradas en registros
(líneas) y campos (columnas), que se vinculan entre sí por un campo en común, en ambos
casos posee las mismas características como por ejemplo el nombre de campo, tipo y longitud
Modelo relacional: es un modelo de datos basado en la lógica de predicados y en la teoría de
conjuntos.
* <strong>Aplicación web:</strong> Se denomina aplicación web a aquellas herramientas que los usuarios
pueden utilizar accediendo a un servidor web a través de internet
* <strong>App:</strong> Se refieren sobre todo a aplicaciones destinadas a tablets (como el iPad o equipos
Android) o a teléfonos del tipo smartphone (como el iPhone o el Samsung Galaxy).
* <strong>Módulo:</strong> Elemento con función propia concebido para poder ser agrupado de distintas maneras
con otros elementos constituyendo una unidad mayor.
* <strong>Caso de uso:</strong> Es una descripción de las actividades que deberá realizar alguien o algo para
llevar a cabo algún proceso.
* <strong>Diagrama de casos de uso:</strong> Representa gráficamente cada caso de uso del sistema.
* <strong>Actores:</strong> Personajes o entidades que participan en un diagrama de caso de uso.
* <strong>Especificación de alto nivel:</strong> Describe brevemente cada caso de uso, y sus actores.
* <strong>Especificación extendida:</strong> Es una descripción mas detallada de los pasos o posibles pasos
que debe ejecutar el actor en cada caso de uso.
* <strong>Flujo normal:</strong> Son los pasos que el sistema y el actor tienen que ejecutar.
* <strong>Flujo alterno:</strong> Son los posibles errores que puede tener en un determinado paso.


* <h2>Metodología  a utilizar<h2>
Metodología Agíl Scrum
Se selecciona la metodologia Scrum dado a que esta metodología se ajusta perfectamente a el desarrollo de muchos tipos de software.
* Ya que nos permite poder cambiar los requisitos en cualquier punto del desarrollo del sistema sin afectar en la implementación
* Se podrá avanzar de forma rápida en la realización de pequeñas tareas, ya que cada sprint es iterativo e incremetnal, por lo cual facilita el tiempo de desarrollo del mismo
* Esta metodología nos permitirá fijar aquellos requerimientos más necesarios y que generar valor al cliente, para asi priorizarlos y definirlos como terminados de forma más rápida.
* Gracias a los sprints retrospective podremos llevar un mejor control de los problemas detectados en el desarrollo de tareas asignadas y buscar una solución más rápida de la misma
* Podremos generar un esquema de fijar el tiempo y coste de entrega y variar unicamente el alcance, esto retomando en realizar primero los modulos que generar valor al cliente


* <h2>Modelo de branching<h2>
GitFlow
Ya que nos permite llevar un mayor y mejor control de cada versión, y más por si el proyecto requiere de distintos desarrolladores trabajando en el mismo, este modelo de branching nos permite 
llevar un mejor orden, control del código , si uno o más desarrolladores trabajan en un mismo modulo, tambien por el majeno de release y pases a producción, cuando el sistema se encuentre 
en un punto optimo para el mismo. Esto agregando que GitFlow nos permite llevar un mejor control de los bugs encontrados ya sea en desarrollo o en producción , realizarlos de forma ordenada sin afectar
ningún otro proceso.


* <h2>Toma de requerimientos<h2>
<h3>Requerimientos Funcionales</h3>

* El sistema contará con un inicio de sesión
* El sistema debe manejar roles de usuario
* El administrador deberá registrarse de forma individual en el modulo admnistrador
* El sistema contará con modulo de registro de usuarios turistas o entidades terciarias
* El administrador podrá ingresar vuelos disponibles
* El administrador podrá cargar de forma masiva vuelos disponibles
* La aplicaición debe mostrar los vuelos disponibles
* El usuario podrá buscar los vuelos disponibles dependiendo pais de destino
* El sistema debe ser capaz de realizar una validación de esquema de vacunación turista vs permitido en el país destino
* El usuario podrá realizar la compra del vuelo deseado
* El usuario podrá solicitar tipo de vuelo ida y vuelta o unicamente ida
* El usuario podrá reservar renta de autos
* El usuario podrá registrar la reseña respecto al servicio de vuelos
* El sistema debe mostrar reseñas dependiendo el servicio 
* El sistema debe mostrar catalogo de autos disponibles para rentas
* El sistema debe filtrar autos disponibles para rentas por país de destino deseado
* El usuario podrá realizar la compra de la renta de auto
* La entidad de renta de autos podrá ingresar autos disponibles para renta de forma manual
* La entidad de renta de autos podrá ingresar autos disponibles para renta por medio de carga masiva
* El usuario podrá registrrar la reseñea respecto al servicio de renta de autos
* El sistema debe mostrar catálogo de hoteles disponibles 
* El sistema debe filtrar hoteles disponibles por pais , ciudad , cantidad de personas, rango de precios
* El usuario de entidad de hoteles podrá ingresar sus sucursales por ciudad, rango de precios y habitaciones
* El usuario de entidad de hoteles podrá registrar en calendario las fechas disponibles de reservación en el mismo
* El usuario turista podrá comprar reservaciones de hotel
* El usuario debe registrar las vacunas administradas para generar esquema de vacunación
* El sistema debe generar esquema de vacunación por medio de QR
* El administrador podrá ingresar esquema de vacunación necesario dependiendo el pais 
* El usuario podrá consultar esquema de vacunación necesario por tipo de pais
* El administrador pordrá editar los esquemas de vacunación por pais
* El usuario podrá editar esquema de vacunación 

<h3>Requerimientos no funcionales</h3>

* Se debe mostrar mensajes de error para orientar al usuario
* El sistema debe ser compatible en cualquier SO
* Escalabilidad de aplicación
* Seguridad de request realizados hacia micro servicios  (encriptación)
* Interfaz intuitiva para los que la utilicen
* Interfaz con un diseño agradable a la vista
* Interfaz responsive a los diferentes dispositivos en la que se utiliza.
* Seguridad transaccional
* Estabilidad del servicio


<h3>Casos de Uso</h3>
<h4>Diagramas</h4>
<h4>Modulo inicio de sesión</h4>

![title](images/img1.PNG)

<h4>Modulo de registro</h4>

![title](images/img2.PNG)
<h4>Modulo administrador</h4>

![title](images/img3.PNG)

<h4>Modulo de hoteles</h4>

![title](images/img4.PNG)

<h4>Modulo de vuelos</h4>

![title](images/img5.PNG)
<h4>Modulo de renta de autos</h4>

![title](images/img6.PNG)
<h4>Modulo pasaporte COVID</h4>

![title](images/img7.PNG)

<h4>Modulo Bancario</h4>

![title](images/img8.PNG)

<h3>Especificaciones de alto nivel</h3>
<table>
    <thead>
        <tr>
            <td><b>001</b></td>
            <td colspan="2"><b>Registro usuario tipo turista</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario tipo turista podrá registrarse dentro del sistema</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">Ninguno</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Turista: Ingresar Nombre</li>
                    <li>Turista: Ingresar Correo</li>
                    <li>Turista: Ingresar UserName</li>
                    <li>Turista: Ingresar Constraseña</li>
                    <li>Turista: Ingresar nuevamente contraseña para validación</li>
                    <li>Sistema: Verifica que los campos estén correctamente llenos [FA1]</li>
                    <li>Sistema: Manda petición post hacia micro servicio</li>
                    <li>Sistema: Valida que el usuario no exista [FA2]</li>
                    <li>Sistema: Ingresa datos a la Base de datos [FA3]</li>
                    <li>Sistema: Muestra mensaje de usuario creado </li>
                    <li>Sistema: Redirecciona a la página de inicio</li>
                </ol>
            </td>
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                <b>FA1- Sistema lanza error de algún dato mal ingresado, el usuario regresa al paso 1 del flujo normal</b><br><br>
                <b>FA2- Sistema lanza error de usuario ya existente, el usuario regresa al paso 1 del flujo normal</b><br><br>
                <b>FA3- Error con la conexión a base de datos</b><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>002</b></td>
            <td colspan="2"><b>Registro usuario tipo servicios tercerizados</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario tipo servicios tercerizados podrá registrarse dentro del sistema</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo tercerizados</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">Ninguno</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Usuario: Ingresar Nombre de Entidad</li>
                    <li>Usuario:Ingresar Correo</li>
                    <li>Usuario: Ingresar UserName</li>
                    <li>Usuario: Ingresar Constraseña</li>
                    <li>Usuario: Ingresar nuevamente contraseña para validación</li>
                    <li>Usuario: Seleccionar tipo de entidad</li>
                    <li>Sistema: Verifica que los campos estén correctamente llenos [FA1]</li>
                    <li>Sistema: Manda petición post hacia micro servicio</li>
                    <li>Sistema: Valida que el usuario no exista [FA2]</li>
                    <li>Sistema: Ingresa datos a la Base de datos [FA3]</li>
                    <li>Sistema: Muestra mensaje de usuario creado </li>
                    <li>Sistema: Redirecciona a la página de inicio</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                <b>FA1- Sistema lanza error de algún dato mal ingresado, el usuario regresa al paso 1 del flujo normal</b><br><br>
                <b>FA2- Sistema lanza error de usuario ya existente, el usuario regresa al paso 1 del flujo normal</b><br><br>
                <b>FA3- Error con la conexión a base de datos</b><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>003</b></td>
            <td colspan="2"><b>Registro usuario tipo administrador</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario tipo administrador podrá registrarse dentro del sistema</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo administrador</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">Inicio de sesión de usuario tipo administrador</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Usuario: Inicia sesión como administador</li>
                    <li>Usuario: Ingresa a opción de registro de usuario adminstrador</li>
                    <li>Usuario: Ingresar Nombre</li>
                    <li>Usuario: Ingresar UserName</li>
                    <li>Usuario: Ingresar Constraseña</li>
                    <li>Usuario: Ingresar nuevamente contraseña para validación</li>
                    <li>Sistema: Verifica que los campos estén correctamente llenos [FA1]</li>
                    <li>Sistema: Manda petición post hacia micro servicio</li>
                    <li>Sistema: Valida que el usuario no exista [FA2]</li>
                    <li>Sistema: Ingresa datos a la Base de datos [FA3]</li>
                    <li>Sistema: Muestra mensaje de usuario creado </li>
                    <li>Sistema: Redirecciona a la página de inicio</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                <b>FA1- Sistema lanza error de algún dato mal ingresado, el usuario regresa al paso 1 del flujo normal</b><br><br>
                <b>FA2- Sistema lanza error de usuario ya existente, el usuario regresa al paso 1 del flujo normal</b><br><br>
                <b>FA3- Error con la conexión a base de datos</b><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>003</b></td>
            <td colspan="2"><b>Iniciar sesión</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario previamente registrado, podrá iniciar sesión dentro del sistema</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo administrador,turista y servicio terciario</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">Registar usuario</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Usuario: Ingresar User Name</li>
                    <li>Usuario: Ingresar contraseña</li>
                    <li>Usuario: Click en iniciar sesión</li>
                    <li>Sistema: Verifica que los campos estén correctamente llenos [FA1]</li>
                    <li>Sistema: Manda petición get hacia micro servicio [FA2]</li>
                    <li>Sistema: Verificar que el usuario con los datos ingresados exista [FA3]</li>
                    <li>Sistema: Retorna la información para el manejo de la sesión del usuario</li>
                    <li>Sistema: Redirecciona al módulo correspondiente al tipo de usuario</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                <b>FA1- Sistema lanza error de algún dato mal ingresado, el usuario regresa al paso 1 del flujo normal</b><br><br>
                <b>FA2- Error de conexión a micro servicio, el usaurio regresa al paso 1</b><br><br>
                <b>FA3- Error el usuario no existe, el usuario regresa a pagina de inicio</b><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>



<table>
    <thead>
        <tr>
            <td><b>004</b></td>
            <td colspan="2"><b>Crear calendario de reservaciones</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario crea dentro de un calendario todas las posibles reveresvaciones</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo entidad hotel</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">Iniciar sesión como usuario entidad hotel</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Usuario: Ingresar a opcion de crear reservaciones</li>
                    <li>Sistema: Despliega calendario</li>
                    <li>Usuario: Selecciona dias posibles para reserva</li>
                    <li>Usuario: Click en guardar reservas</li>                    
                    <li>Sistema: Realiza petición post hacia micro servicio [FA1]</li>
                    <li>Sistema: Guarda fechas de reserva del hotel en la bd [FA2]</li>
                    <li>Sistema: Muestra mensaje de fechas guardadas exitosamente</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                <b>FA1- Error de conexión a micro servicio, el usaurio regresa al paso 1</b><br><br>
                <b>FA2- Error con la conexión de base de datos</b><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>



<table>
    <thead>
        <tr>
            <td><b>005</b></td>
            <td colspan="2"><b>Ver reservaciones</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario podrá visualizar las fechas de reservaciones disponibles</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario Turista, entidad hotel</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">- Iniciar sesión como usuario como entidad hotel o turista <br> -Tener calendario de reservaciones previamente registrados</td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
                <ol>
                    <li>Usuario: Ingresar a opcion consultar reservaciones disponibles</li>
                    <li>Sistema: Despliega calendario de reservas [FA1]</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                <b>FA1- Error de conexión a micro servicio que retorna las fechas de reservas</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>006</b></td>
            <td colspan="2"><b>Reservar Hotel</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario podrá reservar un hotel</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
            <ol>
                    <li>Contar con fechas de reservación del hotel</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Ingresar a opcion reservar hotel</li>
                    <li>Usuario: seleccionar hotel y fecha de reserva</li>
                    <li>Sistema: Redirecciona a modulo de pago</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                  No aplica
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>007</b></td>
            <td colspan="2"><b>Buscar hotel por filtro</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario podrá buscar por filtro de pais, ciudad, cantidad de personas, rango de precios , rango de fechas</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
            <ol>
                    <li>Iniciar sesión como usuario</li>
                    <li>Contar con hoteles registrados</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Ingresar a opción de buscar hotel</li>
                    <li>Usuario: Ingresar dato de busqueda</li>
                    <li>Sistema: Retrona hoteles con los parámetros ingresadsos [FA1]</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que retorna los hoteles que cumplan con el registro</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <td><b>008</b></td>
            <td colspan="2"><b>Registrar reseña</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario podrá ingresar una reseña respescto al servicio al servicio utilizado sea hotel,vuelo,renta de auto</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
            <ol>
                    <li>Iniciar sesión como usuario turista</li>
                    <li>Haber utilizado el servicio</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Ingresar a opción añadir reseña dependiendo del servicio</li>
                    <li>Usuario: Click en publicar</li>
                    <li>Sistema: Realiza petición POST hacia micro servicio guardar reseña [FA1]</li>
                    <li>Sistema: Guarda reseña en BD [FA2]</li>                    
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que guarda las reseñas el usuario es retornado al paso 1</b><br><br>
                                  <b>FA2- Error con la conexión a base de datos, el usuario es retornado al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>009</b></td>
            <td colspan="2"><b>Ver reseñas</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá ver las reseñas hechas hacia un tipo de servicio</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista,hotel,renta,administrador</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Sistema: Petición get hacia micro servicio que retorna reseñas hechas dependiendo el servicio</li>
                    <li>Sistema: Despliega reseñas de servicios [FA1]</li>                    
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que retorna las reseñas, el sistema no mostrará ninguna reseña</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>010</b></td>
            <td colspan="2"><b>Crear vuelos</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá crear vuelos</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo administrador</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como administrador</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Ingresar pais y lugar de vuelo</li>
                         <li>Usuario: Ingresar fuechas de vuelo</li>
                         <li>Usuario: Seleccionar tipo de esquema de vacunación</li>
                         <li>Usuario: Click en guardar</li>
                    <li>Sistema: Validación de campos ingresados [FA1]</li> 
                    <li>Sistema: Petición POST hacia micro servicio para guardar datos de vuelo [FA2]</li>                                
                    <li>Sistema: Guarda información de vuelo en BD [FA3]</li>   
                    <li>Sistema: Muestra mensaje al usuario de registro de vuelo exitoso</li>                            
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Campos no llenados, usuario retona al paso 1</b><br><br>                
                                  <b>FA2- Error de conexión a micro servicio que permite guardar información de vuelos usuario retorna al paso 1</b><br><br>
                                  <b>FA3- Error de conexión a base de datos</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>011</b></td>
            <td colspan="2"><b>Cargar vuelos</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá cargar un archivo que contenga los vuelos para no ingresar uno a uno</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo administrador</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como administrador</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Seleccionar opción cargar vuelos</li>
                         <li>Usuario: Subir archivo deseado</li>
                         <li>Sistema: Validar que es un documento valido [FA1]</li>
                         <li>Sistema: Realizar petición post con cada registro de vuelo hacia micro servicio de registro de vuelos [FA2]</li>                             
                    <li>Sistema: Guarda información de vuelos en BD [FA3]</li>   
                    <li>Sistema: Muestra mensaje al usuario de registros de vuelos exitoso</li>                            
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- El documento no es valido, sistema lanza mensaje de documento no válido</b><br><br>                
                                  <b>FA2- Error de conexión a micro servicio que permite guardar información de vuelos usuario retorna al paso 1</b><br><br>
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>012</b></td>
            <td colspan="2"><b>Ver vuelos</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá ver los vuelos disponibles</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista, administrador</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista o administrador</li>
                    <li>Disponer de vuelos ingresadsos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Selecciona opción de consultar vuelos</li>
                         <li>Sistema: Realiza petición get hacia el micro servicio de vuelos [FA1]</li>
                         <li>Sistema: Despliega vuelos disponibles [FA2]</li>                          
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que retorna los vuelos disponibles, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <td><b>012</b></td>
            <td colspan="2"><b>Buscar vuelos</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá buscar vuelos disponibles por país de destino</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                    <li>Disponer de vuelos ingresadsos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Selecciona opción de consultar vuelos</li>
                    <li>Usuario: Ingresa al buscador</li>
                    <li>Usuario: Ingresa parámetro de busqueda</li>
                         <li>Sistema: Realiza petición get hacia el micro servicio de vuelos [FA1]</li>
                         <li>Sistema: Despliega vuelos disponibles según parámetro [FA2]</li>                          
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que retorna los vuelos disponibles, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>



<table>
    <thead>
        <tr>
            <td><b>013</b></td>
            <td colspan="2"><b>Comprar vuelo</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá comprar boletos de vuelo</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                    <li>Seleccionar vuelo deseado</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Selecciona opción comprar vuelo</li>
                    <li>Sistema: Redirecciona al usuario al modulo de compra</li>                        
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                               No aplica
                </td>
            </tr>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <td><b>014</b></td>
            <td colspan="2"><b>Consultar mis vuelos</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá consultar su vuelo adrquirido previamente</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                    <li>Disponer de vuelos adquiridos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Selecciona opción de consultar mis vuelos</li>
                    <li>Sistema: Realiza petición get hacia el micro servicio de vuelos adquiridos por usuario [FA1]</li>
                    <li>Sistema: Despliega vuelos adquiridos[FA2]</li>                          
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que retorna los vuelos adquiridos, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1 y muestra mensaje de intentar más tarde</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>

<table>
    <thead>
        <tr>
            <td><b>015</b></td>
            <td colspan="2"><b>Ingresar autos</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá ingresar información del auto para rentar</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo entidad renta autos</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como entidad renta autos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                         <li>Usuario: Ingresa marca de auto</li>
                         <li>Usuario: Ingresa imagen del auto</li>
                         <li>Usuario: Ingresa placas</li>
                         <li>Usuario: Ingresa modelo de auto</li>
                         <li>Usuario: Ingresa modelo de auto</li>
                         <li>Usuario: Ingresa costo de renta por tiempo determinado</li>
                         <li>Usuario: Click en registrar auto</li>
                    <li>Sistema: Validación de campos ingresados [FA1]</li> 
                    <li>Sistema: Petición POST hacia micro servicio para guardar datos de auto [FA2]</li>                                
                    <li>Sistema: Guarda información del auto en BD [FA3]</li>   
                    <li>Sistema: Muestra mensaje al usuario de registro de auto exitoso</li>                            
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Campos no llenados, usuario retona al paso 1</b><br><br>                
                                  <b>FA2- Error de conexión a micro servicio que permite guardar información de vuelos usuario retorna al paso 1</b><br><br>
                                  <b>FA3- Error de conexión a base de datos</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>016</b></td>
            <td colspan="2"><b>Cargar autos</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá cargar un archivo que contenga información de autos para no ingresar uno a uno</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo entidad renta autos</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como entidad renta autos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Seleccionar opción cargar autos</li>
                         <li>Usuario: Subir archivo deseado</li>
                         <li>Sistema: Validar que es un documento valido [FA1]</li>
                         <li>Sistema: Realizar petición post con cada registro de auto hacia micro servicio de registro de auto [FA2]</li>                             
                    <li>Sistema: Guarda información de autos en BD [FA3]</li>   
                    <li>Sistema: Muestra mensaje al usuario de registros de autos exitoso</li>                            
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- El documento no es valido, sistema lanza mensaje de documento no válido</b><br><br>                
                                  <b>FA2- Error de conexión a micro servicio que permite guardar información de autos usuario retorna al paso 1</b><br><br>
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>017</b></td>
            <td colspan="2"><b>Comprar renta de auto</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá comprar la renta de un auto</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                    <li>Seleccionar auto deseado</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol> 
                    <li>Usuario: Selecciona opción comprar renta de auto</li>
                    <li>Usuario: Ingresa tiempo de renta</li>
                    <li>Sistema: Calcula precio por tiempo</li>
                    <li>Sistema: Redirecciona al usuario al modulo de compra</li>                        
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                               No aplica
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>017</b></td>
            <td colspan="2"><b>Comprar renta de auto por reserva</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá comprar la renta de un auto por reserva</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                    <li>Seleccionar auto deseado</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol> 
                    <li>Usuario: Selecciona opción comprar renta de auto</li>
                    <li>Usuario: Ingresa tiempo de renta</li>
                    <li>Usuario: Selecciona la fecha en la cual desea reservar</li>
                    <li>Sistema: Calcula precio por tiempo</li>
                    <li>Sistema: Válida que no se encuentre el auto reservado para la fecha deseada</li>
                    <li>Sistema: Redirecciona al usuario al modulo de compra</li>                        
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                               No aplica
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>018</b></td>
            <td colspan="2"><b>Ver autos disponibles para renta</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá ver los autos disponibles</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                    <li>Disponer de autos ingresadsos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                         <li>Usuario: Selecciona opción de consultar autos</li>
                         <li>Sistema: Realiza petición get hacia el micro servicio de autos [FA1]</li>
                         <li>Sistema: Despliega autos disponibles [FA2]</li>                          
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que retorna los autos disponibles, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>019</b></td>
            <td colspan="2"><b>Buscar auto por pais</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso,el usuario podrá buscar autos disponibles por país de destino</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                    <li>Disponer de autos ingresadsos</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Selecciona opción de consultar autos</li>
                    <li>Usuario: Ingresa al buscador</li>
                    <li>Usuario: Ingresa parámetro de busqueda</li>
                         <li>Sistema: Realiza petición get hacia el micro servicio de autos [FA1]</li>
                         <li>Sistema: Despliega autos disponibles según parámetro [FA2]</li>                          
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que retorna los autos disponibles, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>020</b></td>
            <td colspan="2"><b>Registrar esquema de vacunación</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario podrá regsitrar los esquemas de vacunación validos dependiendo el país</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo administrador</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como administrador</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Selecciona el país</li>
                    <li>Usuario: Ingresa tipos de vacunas necesarias</li>
                    <li>Usuario: Click ingresar esquema de vacunación</li>
                         <li>Sistema: Realiza petición POST hacia micro servicio de registro de esquema de vacunación [FA1]</li>
                         <li>Sistema: Registra esquema de vacunación en BD [FA2]</li>                          
                         <li>Sistema: Muestra mensaje de registro de esquema de vacunación exitoso</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que registra esquema de vacunación, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>021</b></td>
            <td colspan="2"><b>Registrar esquema de vacunación</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario podrá regsitrar las dósis de vacunación</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Ingresar a opción registrar pasaporte COVID</li>
                    <li>Usuario: Ingresa tipos de vacunas administradas</li>
                    <li>Usuario: Click ingresar esquema de vacunación</li>
                         <li>Sistema: Realiza petición POST hacia micro servicio de registro de esquema de vacunación de usuarios turistas [FA1]</li>
                         <li>Sistema: Registra esquema de vacunación en BD [FA2]</li>                          
                         <li>Sistema: Muestra mensaje de registro de esquema de vacunación exitoso</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que registra esquema de vacunación, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>022</b></td>
            <td colspan="2"><b>Obtener certificado digital</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario podrá obtener código QR con la informoación del esquema de vacunación registrado</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista</li>
                     <li>Haber ingresado previamente un esquema de vacunación</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Ingresar a opción obtener esquema de vacunación</li>
                     <li>Sistema: Realiza petición GET hacia micro servicio de registro de esquema de vacunación de usuarios turistas [FA1]</li>                         
                         <li>Sistema: Muestra código QR con esquema de vacunación</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que registra esquema de vacunación, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA3- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <td><b>023</b></td>
            <td colspan="2"><b>Consultar esquemas de vacunación</b></td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><b>Descripción</b></td>
            <td colspan="2">En este caso, el usuario podrá consultar los esquemas de vacunación necesarios por pais</td>
        </tr>
        <tr>
            <td><b>Actores</b></td>
            <td colspan="2">Usuario tipo turista, tipo administrador</td>
        </tr>
                <tr>
            <td><b>Pre-requisitos</b></td>
            <td colspan="2">
                <ol>
                    <li>Iniciar sesión como turista o administrador</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td><b>Flujo Normal</b></td>
            <td colspan="2">
            <ol>
                    <li>Usuario: Ingresar a opción mostrar esquemas de vacunación</li>
                     <li>Sistema: Realiza petición GET hacia micro servicio de registro de esquema de vacunación por pais [FA1]</li>                         
                     <li>Sistema: Muesrta listado de esquemas de vacunación necesarios dependiendo el pais [FA2]</li>
                </ol>
            </td>  
        </tr>
        <tr>
            <tr>
                <td rowspan="1"><b>Flujo Alterno</b></td>
                <td>
                                  <b>FA1- Error de conexión a micro servicio que registra esquema de vacunación, el sistema muestra mensaje de error y el usuario es redireccionado a inicio de módulo</b><br><br>                
                                  <b>FA2- Error de conexión a base de datos, el sistema retorna al usuario al paso 1</b><br><br>
                </td>
            </tr>
        </tr>
    </tbody>
</table>


<h1>Historias de usuario</h1>


<h4>Login usuario</h4>

* Cómo usuario quiero una vista de inicio de sesión para poder iniciar sesión dentro del sistema

<h4>Registro administrador</h4>

* Como administrador quiero un módulo para poder registrar otro administrador
<h4>Registro usuario</h4>

* Como turista o entidad terciaria necesito módulo de registro para poder registrarme dentro del sistema

<h4>Registro vuelos</h4>

* como administrador quiero ingresar nuevos vuelos disponibles dentro del sistema, para actualizar la lista de datos vuelos disponibles

<h4>Carga masiva vuelos</h4>
* Como administrar quiero una forma de cargar de forma masiva vuelos disponibles, para facilitar el ingreso de los mismos.

<h4>Vista vuelos</h4>

* Como turista quiero un modulo que me permita visualizar todos aquellos vuelos disponibles, para poder elegir el vuelo más conveniente

<h4>Busqueda de vuelos</h4>

* Como turista quiero una forma de buscar los vuelos disponibles por pais , para poder facilitar la busqueda al pais de destino

<h4>Validación esquema de vacunación</h4>

* Como administrador necesito tener la validación del esquema de vacunación del turista vs permitido en el país, para que solo los turistas con esquema de vauncación correcto puedan viajar

<h4>Compra boleto</h4>

* Como turista quiero un modulo para poder realizar la compra de mi boleto de vuelo, para poder realizar el viaje posteriormente

<h4>Tipo de vuelo</h4>

* Como turista quiero poder seleccionar si realizo un tipo de vuelo de ida y vuelta o unicamente ida, para poder realizar solo un tipo de viaje 

<h4>Renta de autos</h4>

* Como turista quiero un modulo para poder reservar renta de autos, para poder tener un auto temporal al llegar al pais de destino

<h4>Reseña renta de autos</h4>

* Como turista quiero un modulo para poder registrar la reseña respecto al servicio de vuelo, para poder comentar sobre como estuvo el servicio como tal

<h4>Vista reseña</h4>

* Como usuario quiero observar las distintas reseñas respecto al servicio de vuelos, para poder saber sobre los comentarios del mismo.

<h4>Vista autos disponibles</h4>

* Como turista quiero visualizar la lista de autos disponibles para la renta, para poder seleccionar el auto que más me convenga.

<h4>Compra renta de autos</h4>

* Como turista quiero un modulo de registro de compra, para  registrar mi compra y realizar el pago para la renta del auto

<h4>Busqueda auto por pais</h4>

* Como turista quiero buscar los autos disponibles para rentar, dependiendo el pais de destino para facilitar la busqueda de autos disponibles.

<h4>Registro autos</h4>

* Como entidad de renta de autos quiero un modulo para poder registrar todos los autos disponibles tanto de forma manual como por carga de datos, para poder así registarlo dentro del sistema y posteriormente que sean visibles para los turistas.

<h4>Registro reseña servicio renta autos</h4>

* Como turista quiero registrar una reseña sobre el servicio de renta de autos para registrar dicho comentario

<h4>Catálogo de hoteles</h4>

* Como turista quiero visualizar el catálogo de hoteles disponible para poder reservar uno

<h4>Busqueda de hoteles</h4>

* Como turista quiero filtrar la busqueda de hoteles por pais, ciudad , cantidad de personas, rango de precios y habitaciones para poder facilitar la busqueda del mismo.

<h4>Registro hoteles</h4>

* Como entidad tipo hotel quiero registrar sucrusales por ciudad, rango de precios y habitaciones para poder mostrarlo a turistas.

<h4>Calendario fechas disponibles</h4>

* Como entidad de hoteles quiero registrar en un calendatio las fechas disponibles de reservación en el mismo, para poder mostrar al turista las fechas disponibles por cada hotel.

<h4>Compra reserva hotel</h4>

* Como usuario tipo turista quiero poder realizar la compra para reservar en el hotel para poder adquirir una reserva.

<h4>Edición esquema de vacunación país</h4>

* Como administrador quiero editar el esquema de vacunación de un país, para poder actualizarlo si en dado caso es necesario

<h4>Edición esquema de vacunación turista</h4>

* Como turista quiero editar mi esquema de vacunación para actualizarlo en refuerzos posteriores 


<h1>Descripción de tecnologias a utilizar</h1>
<h3>Ionic Angular</h3>
Ionic es un open source SDK, multiplataforma, que permite desplegar la aplicación tanto web , cómo movil, se elige el mismo por su versatilidad, y fácil forma de desplegar la misma aplicación en web y móvil y así ahorrar la realización de dos proyectos completamente distintos

Angular ya que es un framework rápido, y propociona una estrctura ordenada de modulos y componentes por lo cual será más fácil el desarrollo del mismo , y la actualización constante de funcionalides

<h3>Python Flask</h3>
Para el desarrollo de los micro servicios necesarios se utilizará Python con librería flask para poder recibir las petciones proporcionadas por el cliente.

<h3>Docker</h3>
Para poder almacenar cada micro servicio realizado y facilitar la conexión hacía el cliente

<h3>google cloud platform</h3>
VM en google cloud platform , para levantar micro servicios y página

<h3>Git Lab</h3>
Para versionamiento de código, tambien porque tiene su propias funciones de automatización

<h3>MySql</h3>
Base de datos relacional, para poder registrar todo lo necesario dentro del sistema, ya que muchas entidades dependen de otras , es más factible que sea relacional.


<h1>Nombre Servicio: Ingresar usuario</h1>
<h2>Descripción</h2>

-   Ruta: {backend_ur}/user
-   Método: POST 
-   Formato de entrada

<h2>Header</h2>

<table>
<tr>
<td>Atributo</td>
<td>Value</td>
</tr>
<tr>
<td>Content-type</td>
<td>aplication/json</td>
</tr>
<tr>
<td>Authentication</td>
<td>Bearer {Token}</td>
</tr>
</table>

<h4>Parámetros de entrada</h4>
<table>
<tr>
<td>Atributo</td>
<td>Tipo</td>
<td>Descripción</td>
</tr>
<tr>
<td>Nombre</td>
<td>String</td>
<td>Nombre del usuario</td>
</tr>
<tr>
<td>Correo</td>
<td>String</td>
<td>Correo del usuario</td>
</tr>
<tr>
<td>Tipo</td>
<td>String</td>
<td>Tipo de usuario</td>
</tr>
<tr>
<td>Contraseña</td>
<td>String</td>
<td>Contraseña del usuario</td>
</tr>
</table>

<h5>Código Respuesta Exitosa</h5>
200
<h5>Código Respuesta Fallida</h5>
400



<h1>Nombre Servicio: Ingresar vuelos</h1>
<h2>Descripción</h2>

-   Ruta: {backend_ur}/vuelos
-   Método: POST 
-   Formato de entrada

<h2>Header</h2>

<table>
<tr>
<td>Atributo</td>
<td>Value</td>
</tr>
<tr>
<td>Content-type</td>
<td>aplication/json</td>
</tr>
<tr>
<td>Authentication</td>
<td>Bearer {Token}</td>
</tr>
</table>

<h4>Parámetros de entrada</h4>
<table>
<tr>
<td>Atributo</td>
<td>Tipo</td>
<td>Descripción</td>
</tr>
<tr>
<td>Pais</td>
<td>String</td>
<td>Pais de destino del vuelo</td>
</tr>
<tr>
<td>Precio</td>
<td>Double</td>
<td>Precio del vuelo</td>
</tr>
<tr>
<td>Ida_vuelta</td>
<td>Boolean</td>
<td>Valor true si es ida vuelta o false si solo ida</td>
</tr>
<tr>
<td>Cantidad</td>
<td>int</td>
<td>Cantidad de vuelos disponibles</td>
</tr>
</table>

<h5>Código Respuesta Exitosa</h5>
200
<h5>Código Respuesta Fallida</h5>
400


<h1>Nombre Servicio: Ingresar pago</h1>
<h2>Descripción</h2>

-   Ruta: {backend_ur}/pago
-   Método: POST 
-   Formato de entrada

<h2>Header</h2>

<table>
<tr>
<td>Atributo</td>
<td>Value</td>
</tr>
<tr>
<td>Content-type</td>
<td>aplication/json</td>
</tr>
<tr>
<td>Authentication</td>
<td>Bearer {Token}</td>
</tr>
</table>

<h4>Parámetros de entrada</h4>
<table>
<tr>
<td>Atributo</td>
<td>Tipo</td>
<td>Descripción</td>
</tr>
<tr>
<td>Usuario</td>
<td>int</td>
<td>llave foranea del usuario</td>
</tr>
<tr>
<td>Tipo_tarjeta</td>
<td>String</td>
<td>Tipo de tarjeta del usuario</td>
</tr>
<tr>
<td>No_tarjeta</td>
<td>int</td>
<td>Número de tarjeta del usuario</td>
</tr>
<tr>
<td>CVV</td>
<td>int</td>
<td>Número cvv de la tarjeta del usuario</td>
</tr>
<tr>
<td>Precio_total</td>
<td>Double</td>
<td>Precio a pagar por el usuario</td>
</tr>

</table>

<h5>Código Respuesta Exitosa</h5>
200
<h5>Código Respuesta Fallida</h5>
400

<h1>Nombre Servicio: Ingresar hotel</h1>
<h2>Descripción</h2>

-   Ruta: {backend_ur}/hotel
-   Método: POST 
-   Formato de entrada

<h2>Header</h2>

<table>
<tr>
<td>Atributo</td>
<td>Value</td>
</tr>
<tr>
<td>Content-type</td>
<td>aplication/json</td>
</tr>
<tr>
<td>Authentication</td>
<td>Bearer {Token}</td>
</tr>
</table>

<h4>Parámetros de entrada</h4>
<table>
<tr>
<td>Atributo</td>
<td>Tipo</td>
<td>Descripción</td>
</tr>
<tr>
<td>Nombre</td>
<td>String</td>
<td>Nombre del hotel</td>
</tr>
<tr>
<td>Ciudad</td>
<td>String</td>
<td>Tipo de tarjeta del usuario</td>
</tr>
<tr>
<td>No_tarjeta</td>
<td>int</td>
<td>Número de tarjeta del usuario</td>
</tr>
<tr>
<td>CVV</td>
<td>int</td>
<td>Número cvv de la tarjeta del usuario</td>
</tr>
<tr>
<td>Precio_total</td>
<td>Double</td>
<td>Precio a pagar por el usuario</td>
</tr>

</table>

<h5>Código Respuesta Exitosa</h5>
200
<h5>Código Respuesta Fallida</h5>
400


<h1>Listado y descripción de Microservicios</h1>

- Registro

    - Microservicio que se encargara del proceso de registro de usuarios dentro del sistema.
   


- Login

    - Microservicio que se encargara de validar las credenciales del usuario al momento de iniciar sesión, tambien de proporcionar JWT al usuario dentro la aplicación.


- Vuelos 

    - Microservicio que permite realizar todas las funcionalidades respecto a vuelos, como lo es crear vualos, obtener vuelos, editar, y crear esquema de vacunación, registro de reseñas y realizar compras mediante el miscroservicio de banco.


- Renta de autos

    - Microservicio que permite realizar todas las funcionalidades respecto a la renta de autos , como lo es obtener autos, ingresar autos, reservar autos, registro de reseñas y realizar compras mediante el miscroservicio de banco.



- Hoteles

    - Microservicio que permite realizar todas las funcionalidades respecto a hoteles como la reservación, registro de reseñas y realizar compras mediante el miscroservicio de banco



- Banco

    - Microservicio que permite realizar la compra de cualquier servicio



- Resultados

    - Microservicio que permite retornar toda la información necesaria para mostrar en los reportes de resultado.
 
<h1>Diagrama de datos de la aplicación</h1>

![title](images/ER.jpg)



<h1>Arquitectura</h1>

![title](images/arquitectura.png)

<h1>Descripción Seguirdad de Aplicación</h1>



![title](images/Seguridad.png)

Modelo de STRIDE

S: Spoofing
R: Repudation
I: Information disclosure
D: Denial of service
E: Elevation of privilege


- Para este caso se utilza el modelo STRIDE  dentro de la arquitrectura, para poder especificar soluciones posibles a las amenzas correspoindientes

- En el modelo stride se habla sobre TLS lo cual son protocolos criptográficos, que proporcionan comuncaciones seguras por una red.

- Transaction log se refiere a todos los logs dentro de la base de datos de cada transacción para asegurar el no repudio


- JWT para mejorar la encriptación de la información que se trasalada entre servicio y para validar la autorización del usuario, esto mediante el secret para validar que es un request por medio de nuestra aplicación.








<h1>Análisis UX/UI</h1>

*   Diseño minimalista para mejorar la experiencia de usuario
*   Filtro de busqueda de todos los posibles sease hoteles, autos y vuelos 
*   Recuperación y notificación de errores al usuario
*   Flexibilidad en el uso de la aplicación
*   Visibilidad del estado del sistema mediante a una notificación si el servicio no está disponible
*   Paginación de la lista de cada uno de los servicios, para mejorar la navegación del usuario


<h1>Mockups</h1>

* Vista registro de usuarios

![title](images/mk1.PNG)

* Vista Login usuario

![title](images/mk2.PNG)

* Vista busqueda de habitación / hotel con filtro , aqui se desplagaran todas las habitaciones con descipción y opción de ingresar a ver el detalle del mismo.

![title](images/mk3.PNG)

* Vista detalle de habitaciones

![title](images/mk4.PNG)

* Vista renta de autos en el cual de desplegan todos los autos disponibles, con posibilidad de busqueda y filtro 

![title](images/mk5.PNG)

* Vista de vuelos en el cual se desplegan todos los vuelos, con posibilidad de busqueda y filtro

![title](images/mk6.PNG)

* Vista que permite registrar las fechas en las cuales puede registrar posibles reservaciones

![title](images/mk7.PNG)

* Vista que permite subir reseña sobre algún servicio .

![title](images/mk8.PNG)

* Vista que permite registrar vacuna para esquema de vacunación

![title](images/mk9.PNG)

* Vista para la realización de pagos.

![title](images/mk10.PNG)

* Vista que permite registrar vacunas para esquema de vacunación para país.

![title](images/mk11.PNG)



<h1>Documentación de pipelines</h1>

![title](images/pipe.PNG)



* <h2>Build</h2>
Permite realizar la construcción de cada proyecto y la generación de los contenedores respectivos.

servicio_hotel
front_end
servicio_vuelos
servicio_renta
servicio_banco
servicio_resultados
servicio_login
servicio_registro
servicio_esb




* <h2>Test</h2>
Cada servicio tendría su respectivo test del proyecto para la validación de que cada función.


* <h2>Upimages</h2>
En este stage se suben las imagenes al repositorio de contenedores


* <h2>Pullimages</h2>
En este stege se obtienen las imagenes y son deployadas en lás maquinas correspondientes